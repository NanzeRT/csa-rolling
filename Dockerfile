FROM ubuntu
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -qy \
    texlive-base \
    texlive-latex-recommended \
    texlive-latex-extra \
    texlive-lang-cyrillic \
    cm-super \
    pandoc \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /data
VOLUME ["/data"]

# docker build --tag latex-pandoc .
# docker run --rm -it -v $PWD:/data latex-pandoc pandoc lecture-notes.md -o csa-notes.pdf

# Source code related to the course

- `data_memory.v` and `data_memory_tb.v` -- part of lab3 scheme in verilog as a detail example of scheme with a state.
    - `cd src && iverilog data_memory.v data_memory_tb.v && ./a.out`
    - `iverilog` -- Icarus Verilog
    - VCD visualization -- GTK Wave
    - Scheme visualization -- [DigitalJS](http://digitaljs.tilk.eu/)

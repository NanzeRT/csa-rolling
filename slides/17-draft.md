# Заметки

## Забытое

- [Барьеры памяти](https://ru.wikipedia.org/wiki/Барьер_памяти) -- вероятно нужно перенести к суперскалярам.
- Общая память (например, у CPU и GPU)
- Интерфейсы периферийных устройств и их классификация.

---

<div class="row"><div class="col">

Встраиваемая/встроенная система (embedded system)
: специализированная микропроцессорная система управления, контроля и мониторинга, концепция разработки которой заключается в том, что такая система будет работать, будучи встроенной непосредственно в устройство, которым она управляет.

</div><div class="col">

![](figures/embedded-system-applications.jpg)

Система на Кристалле (СнК, System-on-a-Chip, SoC)
: электронная схема, выполняющая функции целого устройства (например, компьютера) и размещённая на одной интегральной схеме.

</div></div>

----

![](figures/microservices-vs-monolithic-architecture-diagram.png)

---

## Минутка саморекламы. Проекты. Курсы

Особенности проектов для участия:

- Размещены на GitHub
- Лицензия -- BSD
- Задачи проектов могут стать вашими дипломами и практиками

Подробнее: [Aleksandr Penskoi](https://ryukzak.github.io)

![](figures/qr-penskoi.png)

----

### NITTA

Инструментарий для генерации и программирования специализированных не фон – Неймановских процессоров, предназначенных для циклического исполнения алгоритмов управления и обработки сигналов/данных.

- обеспечить высокую скорость и уровень параллелизма для нерегулярных алгоритмов (где не используют GPU) и жесткое реальное время (с точностью до такта);
- сделать процессор реконфигурируемым под разные прикладные области;
- предоставить язык высокого уровня для прикладных разработчиков и быструю компиляцию.

Технологии: Haskell, Verilog, TypeScript, React, GitHub.

----

### Course-bot (Telegram)

Features:

- Quiz in test form
- Essay submissions by the students with cross-review
- Scheduling in-class presentations, including topic reviews by the teacher, agenda generation, and evaluation by the teacher and students.

Technical details:

- eDSL for describing bot in dialogue form
- Implementing on Clojure
- Possible topics for bachelor’s or master’s thesis:

    - eDSL for dialogue system descriptions
    - Automated testing for dialogue systems based on a property-based approach

----

### Курс <br/> Системная инженерия: Архитектурное моделирование компьютерных систем

> Всё, что может пойти не так, пойдёт не так.

--- закон Мерфи

> Любое дело всегда длится дольше, чем ожидается, даже если учесть закон Хофштадтера.

--- закон Хофштадтера

----

Причины:

- Современные системы имеют высокую сложность и тесно вплетены в непростые бизнес-процессы.
- Их разрабатывает толпа обычных людей, от которых требуется ничего не забыть, ничего не упустить, нигде не ошибиться и договориться между собой.

Данный курс учит:

- базовым приёмам системной инженерии (Как создавать успешные системы?);
- архитектурному документированию (Как записать "всё важное" в виде документа?).

---

[John L. Hennessy, David A. Patterson Новый золотой век для компьютерной архитектуры](https://habr.com/en/post/440760/)

---

#### Параллельный доступ. Offtopic. Consistency

- **Сильная согласованность (Strong consistency)**. После завершения обновления любой последующий доступ к данным вернет обновленное значение.
- **Слабая согласованность (Weak consistency)** / **согласованность в конечном счете (Eventual consistency)**. Система гарантирует, что при отсутствии изменений данных в конечном счёте все запросы будут возвращать последнее обновленное значение. Пример: DNS.

Notes: <https://habr.com/ru/post/100891/>

---

### Диалектика Гегеля

![](figures/hegel-dialecric.png)

----

### Принцип развития иерархических систем Седова

В сложной иерархически организованной системе рост разнообразия на верхнем уровне обеспечивается ограничением разнообразия на предыдущих уровнях, и наоборот, рост разнообразия на нижнем уровне разрушает верхний уровень организации.

![](figures/platform-disaggregation.png)

----

## Формирование уровней

**Уровень компьютерной системы** -- совокупность процессора, вычислительных процессов и моделей. Уровни определяются вне зависимости от конкретной стадии жизненного цикла вычислительной системы и могут включать в себя как архитектурные представления, так и модели, сформированные в процессе отладки.

![*Формирование уровней компьютерной системы*](figures/level-forming.png) <!-- .element height="400px" -->

----

### Layered Style

![](figures/style-layered.png)  <!-- .element: class="fullscreen" -->

----

<div class="row"><div class="col">

![](figures/style-layered-1.png)  <!-- .element: class="fullscreen" -->

![](figures/style-layered-2.png)  <!-- .element: class="fullscreen" -->

</div><div class="col">

![](figures/style-layered-3.png) <!-- .element: height="250px" -->

![](figures/style-layered-4.png) <!-- .element: height="250px" -->

</div></div>

----

### Модель-процесс-вычислитель

<div class="row"><div class="col">

- Модель (конфигурация) вычислительного процесса, описывающая вычислительный процесс в рамках ВПл.
- Вычислитель -- целостный зафиксированный механизм, обеспечивающий вычислительный процесс.
- Вычислительный процесс (ВП) и частичный процесс, которые разворачиваются вычислителем и соответствуют модели по построению или формальному критерию.
- Компонент ВП -- атомарный или составной шаг вычислителя.

</div><div class="col">

![](figures/mvp-1.png)

</div></div>

----

<div class="row"><div class="col">

- Вычислительный механизм (ВМх) -- часть вычислителя, обеспечивающая компонент ВП.
- Отношение виртуализации -- абстракция над процессом, фиксирующая вычислитель.
    - Определяет полное множество атомарных компонентов ВП и позволяет описать любой допустимый ВП.
    - Невыразимость процесса говорит о сбое вычислителя или некорректной виртуализации.
- Отношение трансляции -- формальное соответствие двух моделей друг другу.

</div><div class="col">

![](figures/mvp-2.png)

</div></div>

---

### Ultimate Reduced Instruction Set Computer (URISC)

- Доступна лишь одна инструкция, но есть полнота по Тьюрингу.
- Архитектуру можно рассматривать как предельный случай RISC.
- Такой подход интересен скорее с теоретической точки зрения.

Варианты единственной инструкции:

- **RSSB** -- вычесть и пропустить следующую инструкцию, если вычитаемое было больше уменьшаемого.
- **MOV** -- переслать из первой ячейки во вторую (TTA).
- **BBJ** -- копировать один бит из первого по второму адресу памяти и передать управление на третий адрес. Последовательность инструкций может изменить значение в ячейке, на которую перейдёт управление, значит, можно выполнять вычисления, доступные обычному компьютеру.
- есть и другие.

----

### URISC. Пример

```rssb
; 0 - pc
; 1 - acc
; 2 - zero
; 3 - in
; 4 - out
; at start pc = 5

rssb acc      ; 5: acc = acc - acc = 0
rssb C        ; 6: C = acc = C - acc = 67 - 0 = 67
rssb out      ; 7: out = acc

rssb acc      ; 8: acc = acc - acc = 0
rssb S        ; 9: S = acc = S - acc = 83 - 0 = 83
rssb out      ; 10: out = acc

rssb acc      ; 11: acc = acc - acc = 0
rssb A        ; 12: A = acc = A - acc = 65 - 0 = 65
rssb out      ; 13: out = acc

rssb acc      ; 14: acc = acc - acc = 0
rssb one      ; 15: one = acc = one - acc = 1
rssb ip       ; 16: ip = acc = ip - 1 = 17 - 1 = 16, infinite loop

C rssb 67
S rssb 83
A rssb 65
one rssb 1
```

Программа Hello World: [src/helloworld.rssb](https://gitlab.se.ifmo.ru/computer-systems/csa-rolling/-/blob/master/src/helloworld.rssb)

CGRA -- MIMD с распределённой памятью

---

## TODO

- High-level Language Computer Architecture or language-directed computer design (когда попытались поднять уровень ассемблера);
    - <https://ru.wikipedia.org/wiki/IAPX_432> -- высокоуровневый процессор от Intel
    - <https://en.wikipedia.org/wiki/Jazelle> -- процессор с поддержкой java
    - <https://en.wikipedia.org/wiki/PicoJava> -- picoJava
- tagged architecture (когда попытались учесть на низком уровне типы данных);
- детерминированные по времени процессоры (для гарантий реального времени);
- Coarse-Grained Reconfigurable Arrays (очень странное семейство процессоров)
    - NITTA
    - их классификация из обзора
- специализированные процессоры (эффективный параллелизм под задачу, эффективная работа с кешем, управление точностью вычислений, более эффективное программирование за счет ручной оптимизации под процессор). Связка DSL+ спец процессор

---

## Digital Signal Processor (DSP)

![](figures/proc-dsp.gif)

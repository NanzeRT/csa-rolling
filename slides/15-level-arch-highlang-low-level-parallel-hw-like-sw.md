# Архитектура компьютера

## Лекция 15

### Уровневая организация. <br/> Языки высокого уровня. <br/> Низкоуровневый параллелизм <br/> Программы аппаратного толка

Пенской А.В., 2023

----

### План лекции

- Уровневая организация.
- Языки высокого уровня. Структурное программирование
- Low-Level Parallelism, включая:
    - Суперскалярный процессор
    - VLIW
- Программы аппаратного толка

---

## Уровневая организация

это естественный способ организации вычислительных систем

<div class="row"><div class="col">

Примеры:

1. Lava Flow или уровневая организация курильщика $\longrightarrow$
1. Уровневый архитектурный стиль (Layered Style)
1. OSI Model
1. Уровни организации вычислительного процесса

Количество уровней -- десятки.

</div><div class="col">

![](figures/lava-antipattern.jpeg) <!-- .element: height="500px" -->

</div></div>

----

### Уровневый архитектурный стиль

<div class="row"><div class="col">

![](figures/style-layered.png)  <!-- .element: class="fullscreen" -->

- Изоляция, модифицируемость и переносимость
- Управление сложностью и структура кода разработчикам
- Разделения интересов

</div><div class="col">

(**Layered Style**)

![](figures/style-layered-2.png)

![](figures/style-layered-3.png) <!-- .element: height="210px" -->
![](figures/style-layered-4.png) <!-- .element: height="210px" -->

</div></div>

----

### Open Systems Interconnection (OSI) Model

<div class="row"><div class="col">

Зачем уровни? Обеспечить **модульность** и **вариативность**.

1. Application Layer
1. Presentation Layer
1. Session Layer
1. Transport Layer
1. Network Layer
1. Data Link Layer
1. Physical Layer

Какая это точка зрения показана?

Что неверно в визуализации?

</div><div class="col">

![](figures/osi-meme.png)

</div></div>

----

### Уровни организации выч. процесса

<div class="row"><div class="col">

![](figures/platform-general-view.png)

1. Layered project organization.
1. Frameworks, библиотеки, API.
1. Языки программирования.
1. Операционные системы.
1. Системы команд (ISA), ПЛИС.
1. Виртуализированные ресурсы.

</div><div class="col">

![](figures/vertical-horizontal-arch.png) <!-- .element: height="600px" -->

</div></div>

----

### Разделение на уровни (Disaggregation)

![](figures/platform-disaggregation.png)

<div class="row"><div class="col">

- Интегрированные решения <br/> (in-house). Уровни переплетены и адаптированы.

</div><div class="col">

- Уровни разделены по индустриям/специальностям.
- Эффект масштаба.
- Независимое существование. Закон Мура. [Proebsting's Law](https://proebsting.cs.arizona.edu/law.html)

</div></div>

----

### Смешение уровней. Интеграция

![](figures/platform-levels-mix-for-realtime.jpg)

1. Зачем: для обеспечения системных характеристик: энергопотребление, реальное время, производительность.
1. Смешение уровней $\longrightarrow$ рост связанности и сложности системы.
1. Смешение уровней при разработке.
1. Сквозные инструментальные решения.

----

### Документирование. Граф актуализации

<div class="row"><div class="col">

1. Цепочка преобразований моделей выч. процесса через:
    - стадии жизненного цикла <br/> (Design $\rightarrow$ Dev $\rightarrow$ Use),
    - вычислительные платформы (High- $\rightarrow$ Low-level Langs $\rightarrow$ ISA $\rightarrow$ HW $\rightarrow$ Physic).
1. Ориентированный ациклический граф, вершины -- модели, рёбра -- трансляции/интерпретации.
1. Назначение: анализ инструментальных цепочек.

</div><div class="col">

![](figures/actualization-graph-adv.png) <!-- .element: height="450px" -->

</div></div>

Подробности: [Разработка и исследование архитектурных стилей проектирования уровневой организации встроенных систем](http://fppo.ifmo.ru/?page1=16&page2=52&page_d=1&page_d2=142415)

---

<div class="row"><div class="col">

## Вокруг процессора фон Неймана

1. Языки программирования высокого уровня. Структурное программирование.
1. Параллелизм уровня инструкций (Low-Level Parallelism):
    - Суперскалярность
    - VLIW

</div><div class="col">

![](figures/vertical-horizontal-arch-high-lang-instr-parallelism.png) <!-- .element: height="600px" -->

</div></div>

---

## Языки высокого уровня

Инструкции и Go To $\rightarrow$ Структурные блоки и их последовательность

<div class="row"><div class="col">

1. Естественные элементы для архитектуры фон Неймана:
    - последовательный код,
    - условный оператор,
    - циклы,
    - подпрограмма.
1. Распределение регистров. Выражения.
1. Процедуры, области видимости и автоматическая память.
1. `*` Исключения, состояния и перезапуски.
1. `*` Полиморфизм. Замыкания.

</div><div class="col">

![](figures/structural-programming.png)
![](figures/structural-programming-proc.png) <!-- .element height="200" -->
Подробности: [Курс ФП. История](https://gitlab.se.ifmo.ru/functional-programming/main)
<br/> `*` -- на ваше усмотрение

</div></div>

----

### Распределение регистров. Выражения

![](figures/proc-prog-instruction-selection.png) <!-- .element height="370" -->
![](figures/proc-prog-reg-allocations.png)       <!-- .element height="370" -->

1. Проблемы:
    - кол-во регистров конечно (ARM 15/31, x86 8/16, MIPS 32/32),
    - не все регистры одинаковы (особенно в CISC).
1. Проблема компилятора или программиста низкого уровня.
1. Код не ограничивается выражениями.

----

#### Раскраска регистров

![](figures/register-allocation.png) <!-- .element height="450" -->

<div class="row"><div class="col">

$R_1, R_2$ -- регистры аргументов.

$R_{LK}$ -- возврат, $R_1$ -- результат.

</div><div class="col">

Источник: [Register allocation](https://cs420.epfl.ch/archive/20/c/08_reg-alloc.html)

В статье пример с Lisp.

</div></div>

----

### Подпрограммы. Процедуры

<div class="row"><div class="col">

1. Память выделена статически.
1. Реализация:
    - Через `inline`.
    - Через `goto`.
    - Через `call`, `return`.
1. Зачем:
    - переиспользование машинного кода,
    - оптимизация работы кеша инструкций (код процедуры).
1. Нет реентерабельности.
1. Проблемы: кеширование, сброс регистров, переходы и конвейер.

</div><div class="col">

```c
int *x, *y;
void *SWAP_RET;
int tmp;

swap:
    tmp = *x;
    *x = *y;
    *y = tmp;
    goto SWAP_RET;
```

```c
int tmp;

void swap(int *x, int *y) {
    tmp = *x;
    *x = *y; // прерывание: isr()
    *y = tmp;
}

int a, b;

void isr() {
    a = 1;
    b = 2;
    swap(&a, &b);
}
```

</div></div>

----

#### Реентерабельность. Рекурсивный вызов

<div class="row"><div class="col">

1. Реализация через `call`, `return`.
    - Статическое выделение памяти для каждого входа.
    - Автоматическая память, стек: `push`, `pop` (рекурсия).
1. Зачем:
    - переиспользование машинного кода (run-time),
    - рекурсивные алгоритмы.
1. Проблемы: автоматическая память, утечки данных, перезапись адреса возврата.

```c
void fact(int n) {
    if (n == 0) return 1;
    return n * fact(n - 1);
}
```

</div><div class="col">

```c
struct swap {int* x; int* y; int tmp;};

void swap(struct swap* base) {
    base->tmp = *(base->x);
    *(base->x) = *(base->y);
    *(base->y) = base->tmp;
}

struct swap enter1, enter2;
swap(&enter1);
swap(&enter2);

void swap(int* x, int* y) {
    int tmp; // reserve mem
    tmp = *x;
    *x = *y;
    *y = tmp;
}
```

![](figures/prog-recursive-procedure.png)

</div></div>

---

## Низкоуровневый параллелизм <br/> (Low-Level Parallelism)

("не влияет" на прикладное программирование)

1. **Конвейеризация**. Разбиение выполнения инструкции на последовательность этапов.
1. **Отделение ЦПУ от процессоров ввода/вывода**. К примеру: DMA.
1. **Множественные функциональные узлы в ЦПУ**. Независимые функциональные блоки для арифметических и булевых операций, выполняемых одновременно.
    - Суперскалярный процессор
    - Very Long Instruction Word

---

### Суперскалярный процессор

<div class="row"><div class="col">

Проблемы:

- разные операции выполняются в разное время (сумма, деление);
- простаивание конвейера во время длинных операций.

Решение:

- анализ потока инструкций на лету и автоматическая параллелизация инструкций, прозрачная для программиста.

</div><div class="col">

![Concept of a superscalar processor](figures/superscalar-processor-two-unit.png)

Скалярная величина
: величина, которая может быть представлена числом (целочисленным или с плавающей точкой).

</div></div>

----

#### Суперскалярный процессор. Структура

<div class="row"><div class="col">

1. Инструкции извлекаются в очередь команд по порядку.
1. Из очереди они декодируются и перемещаются на станции резервирования.
1. Станции резервирования эффективно выполняют переупорядочивание:
    - Инструкции выполняются по мере доступности данных.
    - Доступность данных редко совпадает с порядком инструкции в очереди.

</div><div class="col">

![](figures/proc-superscalar-processor-organisation.png)

</div></div>

Notes: 18-600 Foundations of Computer Systems, Carnegie Mellon University, J.P. Shen

----

#### Суперскалярный процессор. Недостатки

1. Конфликты по данным оказывают значительное влияние на производительность и сложность процессора.
1. Высокое энергопотребление.
1. Проблемы детерминированности работы многоядерных процессоров. $\longrightarrow$

#### Суперскалярный процессор. Достоинства

1. Рост производительности. Сглаживание длительности выполнения инструкций.
1. Повышение уровня загрузки ресурсов.
1. Совместимость с существующим машинным кодом.
1. Компилятор может устранить значительное количество конфликтов за счёт сортировки инструкций.

----

#### Барьеры памяти (Fence)

<div class="row"><div class="col">

Барьер памяти
: вид барьерной инструкции, которая приказывает компилятору (при генерации инструкций) и центральному процессору (при исполнении инструкций) устанавливать строгую последовательность между обращениями к памяти до и после барьера.

Все обращения к памяти перед барьером будут гарантированно выполнены до первого обращения к памяти после барьера.

</div><div class="col">

```с
// Processor 1:
while (f == 0);
// Memory fence required here
print(x);

// Processor 2:
x = 42;
// Memory fence required here
f = 1;
```

![](figures/barrier-effect-example.png)

</div></div>

---

### Very Long Instruction Word (VLIW)

<div class="row"><div class="col">

RISC: упростим процессор за счёт языков высокого уровня!

VLIW: упростим процессор за счёт переноса параллелизма инструкций в compile-time!

1. Объединим несколько инструкций в одну.
1. Уберём из процессора механизмы перестановки инструкций.
1. Компилятор имеет полный доступ к инструкциям $\longrightarrow$ может "выжать" весь возможный параллелизм.

</div><div class="col">

![](figures/superscalar-and-vliw-scheme.png)

</div></div>

----

#### VLIW. Система команд и устройство

![](figures/vliw-isa.png)

----

<div class="row"><div class="col">

#### VLIW. Достоинства

1. Упрощение процессора, снижение энергопотребления.
1. Упрощение декодера. Рост частоты.
1. Компилятор имеет больше информации о коде, он лучше знает, что параллельно!

</div><div class="col">

#### VLIW. Недостатки

1. Сложность компилятора.
1. Высокая нагрузка на каналы данных и регистровые файлы.
1. Конфликты конвейера приводят к простою всех узлов.
1. Низкая плотность кода.
1. Ширина команды -- огр. микроархитектуры.

</div></div>

![](figures/cisc-risc-vliw-isa.png)    <!-- .element: height="250px" -->

Notes: <https://www.isi.edu/~youngcho/cse560m/vliw.pdf>

---

### VLIW vs. Superscalar

![](figures/proc-superscalar-vs-vliw.jpg)<!-- .element height="600" -->

----

### Уровневая организация. <br/> Суперскаляр $\longleftrightarrow$ VLIW

![](figures/proc-superscalar-epic-vliw.png)

Notes: Understanding EPIC Architectures and Implementations, Mark Smotherman, <https://people.computing.clemson.edu/~mark/464/acmse_epic.pdf>

---

## Программы аппаратного толка

<div class="row"><div class="col">

Некоторые разновидности программируемых компьютеров функционируют как их аппаратные прототипы. Среди них:

1. Программируемые Логические Контроллеры (ПЛК)
1. Программируемые Логические Интегральные Схемы (ПЛИС)

</div><div class="col">

![](figures/vertical-horizontal-arch-fpga-plc.png)

</div></div>

---

### Программируемые логические контроллеры

<!-- TODO: MDE and xtUML overview -->

<div class="row"><div class="col">

Автоматизация при помощи релейных схем:

- плохо масштабируется,
- сложная настройка и поддержка,
- надёжность сложных схем,
- проблема соответствия документации и факта.

![](figures/plc-photo.jpg)<!-- .element: height="200px" -->

</div><div class="col">

ПЛК
: специальная разновидность электронной вычислительной машины.

Особенности:

- являются законченными и самостоятельными устройствами;
- работа в реальном времени;
- эксплуатация в тяжёлых условиях;
- ориентированы на работу с машинами и механизмами.

</div></div>

----

#### Типовое устройство ПЛК

![](figures/plc-typical-internal.jpg)

Также существуют и полностью программные реализации.

----

#### Программирование ПЛК

<div class="row"><div class="col">

IEC 61131
: standard for programmable controllers

Определяет языки для специалистов АСУТП:

- Ladder Diagram (Релейно-контактные схемы)
- Function Block Diagram (Функциональные блоковые диаграммы)
- Sequential Function Chart (Последовательностные функциональные диаграммы)
- Structured Text (Структурированный текст)

</div><div class="col">

![](figures/plc-langs.jpg)

IEC 61499 -- Standard for distributed Automation. Рассмотрим ближе к концу курса в контексте распределённых платформ.

</div></div>

---

### Программируемые логические интегральные схемы

<div class="row"><div class="col">

ПЛИС
: интегральная микросхема, используемая для создания конфигурируемых цифровых электронных схем.

- Логика работы задаётся посредством программирования, а не при производстве.
- Для формирования конфигурации используется специальное ПО и HDL: Verilog, VHDL...
- "[Ре]конфигурируется", а не "программируется".

</div><div class="col">

![](figures/fpga-design-flow.jpg) <!-- .element: height="550px" -->

</div></div>

----

<div class="row"><div class="col">

#### ПЛИС/СБИС (FPGA/ASIC)

![](figures/fpga-vs-asic-cost.png)

[Источник](https://towardsdatascience.com/introduction-to-fpga-and-its-architecture-20a62c14421c)

</div><div class="col">

#### ПЛИС. Этапы синтеза

![](figures/fpga-synthesis.jpg)

</div></div>

----

<div class="row"><div class="col">

#### ПЛИС. Устройство

![](figures/fpga-internal.png)

</div><div class="col">

#### ПЛИС. Lookup tables (LUT)

![](figures/fpga-lut.png)

</div></div>

----

*Questions*:

1. Можно ли это назвать программированием (ПЛК, ПЛИС)?
1. Почему ПЛИС считают аппаратурой и схемотехникой?
1. В чём отличие от "классических программ"?

---

### Высокоуровневый синтез /1 <br/> (High-Level Synthesis)

<div class="row"><div class="col">

Проблемы:

1. Существуют варианты реализаций алгоритма в цифровую схему.
1. Выбор зависит от ограничений, накладываемых на схему.
1. Изменение ограничений может вызвать перепроектирование.
1. Разработка схем трудоёмка и требует специалистов.

</div><div class="col">

![](figures/hls-tradeoff-examples.png)

</div></div>

----

#### Высокоуровневый синтез /2

![](figures/hls-design-flow.png)

----

#### Высокоуровневый синтез. Достоинства

1. Скорость проектирования. Быстрое прототипирование.
1. Переносимость между разными целевыми платформами.
1. Адаптируемость микроархитектуры под новые условия.
1. Автоматизация процесса оптимизации схемы.

#### Высокоуровневый синтез. Недостатки

1. Ограниченный контроль за результатом синтеза. "Чудеса"
1. Кривая обучения (специальность HLS Engineer)
1. Уровень зрелости технологии сильно варьируется:
    - верификация результата;
    - стабильность работы.

---

### Другие инструменты для построения аппаратуры

<!-- TODO: make an overview slides for this tech and comparing with alternatives -->

- Chisel
- Clash
- Bluespec

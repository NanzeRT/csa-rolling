# Заметки к экзаменационным билетам

- Старина RMS

[^ref-levenchuk]: Anatoly Levenchuk "Towards a Systems Engineering Essence." arXiv preprint arXiv:1502.00121 (2015).

[^ref-key]: Alan Key, et al. STEPS Toward The Reinvention of Programming, VPRI Technical Report TR-2007-008

[^ref-essence]: Essence -- Kernel and Language for Software Engineering Methods. Foundation for the Agile Creation and Enactment of Software Engineering Methods (FACESEM) RFP, 2012.

[^ref-cgra-survey]: Liu, Leibo, Jianfeng Zhu, Zhaoshi Li, Yanan Lu, Yangdong Deng, Jie Han, Shouyi Yin, and Shaojun Wei. "A survey of coarse-grained reconfigurable architecture and design: Taxonomy, challenges, and applications." ACM Computing Surveys (CSUR) 52, no. 6 (2019): 1-39.

[^ref-pae-2021]: Алексей Платунов, Василий Пинкевич. "Создание киберфизических систем: проблемы подготовки ИТ-специалистов." Control Engineering, Россия, #3 (93) - 2021

[^ref-cpo]: Aleksandr Penskoi, Computational Process Organisation, Course notes, 2021

[^ref-gielingh]: Gielingh, W., 2008. A theory for the modelling of complex and dynamic systems. Journal of Information Technology in Construction (ITcon), 13(27), pp.421-475.

[^ref-iso15288]: ISO/IEC 15288 Systems and software engineering -- System life cycle  processes. 2008.

[^ref-partridge]: Partridge, C., Business Objects: Re-engineering for re-use. 1996.

[^ref-booch]: Booch Gradu, Maksimchuk Robert A., Engle Michael W. et all. Object-Oriented Analysis and Design with Applications (3rd Edition). Addison-Wesley Professional; 3 edition, 2007

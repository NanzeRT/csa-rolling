# Предметная область

## Компьютерная система

На сегодняшний день основная часть современных систем -- это компьютеры. Например, микроволновая печь с множеством кнопок и пользовательским интерфейсом космического корабля -- это скорее компьютер, чем физическое устройство (например, моя микроволновая печь для простой работы требует выполнения следующей последовательности действий: выбрать режим, установить таймер и запустить фактическую работу). Мы не обсуждаем эти тенденции, но это наша текущая реальность, и мы должны это учитывать. Современная система -- это неоднородная система со сложным поведением, которое не предсказуемо без учета кибернетической части и логики работы компьютера. Приведем другие примеры:

- светодиодная лампа, которая включает в себя контроллер для контроля напряжения;
- кабели для зарядки мобильных телефонов содержат контроллер, который позволяет устройствам проверять совместимость на предмет безопасной зарядки или блокировки от производителя;
- часы и, конечно, умные часы;
- интерактивные детские игрушки;
- автомобиль и беспилотный автомобиль;
- станок;
- дверной замок;
- и т.д.

Все они содержат свои вычислительные части, которые управляют "пассивными" компонентами и объединяют их в систему.

Система -- это суперпозиция множества вещей с другой точки зрения. Иногда мы можем сказать, что компьютер является точкой соединения системы, где разработчики должны соединить все компоненты в единый целостный объект -- систему. Она, как и тело, представляет собой кусок мяса без нейронной системы и мозга.

## Система с преобладающей программной составляющей

Обычно создание современной вычислительной системы требует разработки бизнес-процессов (обучение персонала, пункты управления, регламенты и т.д.) и аппаратного обеспечения (центры обработки данных, кластеры, компьютерные/контроллерные сети, контроллеры, пользовательский IP-код, процессоры и т.д.). Основная часть из них относится к классу систем с преобладающей программной составляющей, а это значит, что значительная часть затрат на систему приходится на разработку программного обеспечения (см. [рисунок](#fig:HW-SW-support-cost)).

![_Тенденция изменения соотношения затрат на компьютерные системы_](../fig/HW-SW-support-cost.png){#fig:HW-SW-support-cost}

Системы с преобладающей программной составляющей
: это системы, в которых разработка и/или интеграция программного обеспечения являются приоритетными аспектами (т.е. наиболее сложные системы в наши дни). К ним относятся компьютерные системы, начиная от отдельных программных приложений, информационных систем, встроенных систем, программных продуктовых линеек и семейств продуктов и систем.
: --- ISO/IEC/IEEE 42010

Software-intensive systems
: are systems in which software development and/or integration are dominant considerations (i.e., most complex systems these days). This includes computer-based systems ranging from individual software applications, information systems, embedded systems, software product lines and product families and systems-of-systems.
: --- ISO/IEC/IEEE 42010

Программная система
: это система, состоящая из программного обеспечения, аппаратного обеспечения и данных, которая обеспечивает свою основную ценность за счет выполнения программного обеспечения.

Software system
: A system made up of software, hardware, and data that provides its primary value by the execution of the software.
: --- OMG Essence

Эта роль программного обеспечения позволяет нам применять стандарты разработки программного обеспечения для компьютерных систем и системной инженерии в целом (с небольшими изменениями [[^ref-levenchuk]]). Одним из важнейших стандартов в этой области является OMG Essence [[^ref-essence]], который определяет основы разработки программного обеспечения и общую точку зрения.

Кроме того, в основном истинно, что сегодня инженеры-программисты обладают одними из самых передовых разработок и инструментов проектирования, поскольку:

- В этой области сосредоточено беспрецедентное количество людей и финансов.
- Обладает самой сложной предметной областью с, возможно, одной из самых сложных формальных конструкций в мире. Полный объем документации (мы обычно называем это реализацией программного обеспечения или программным проектом) для современных программных систем включает миллионы текстовых страниц. Посмотрите этот впечатляющий проект [[^ref-key]], где разработчики пытаются совместить операционную систему с офисным программным обеспечением в одной книге.
- Разработчик программных средств -- разработчик программного обеспечения. Это позволяет им "есть собственный корм для собак" и лучше понимать, как это улучшить.

Эти преимущества делают чрезвычайно важным понимание того, какие принципы, методы и инструменты используются при разработке программного обеспечения. Элементы этих подходов мигрируют в другие сферы и более конкретные области компьютерной инженерии (разработка аппаратного обеспечения, системное программирование и т.д.). Это взаимное влияние значительно улучшает все затронутые области.

Необходимо отдельно отметить, что компьютерные системы (компьютеры) можно условно разделить на две большие группы с точки зрения их назначения:

- информационные компьютерные системы, основные задачи которых -- получить набор данных на вход, преобразовать/накопить его, и выдать в измененном/обработанном виде;
- управляющие компьютерные системы, основная задача которых -- взаимодействовать с реальным физическим миром с целью контроля или управления над ним.

С точки зрения информации, эти классы систем очень похожи, так как в любом случае они преобразуют информацию, но с технической точки зрения они имеют существенные отличия, из-за которых подходы к их разработке и проектированию значительно отличаются. К ключевым отличиям можно отнести два следующих пункта:

- управляющие системы имеют специальное аппаратное исполнение (набор портов ввода-вывода адаптирован под то окружение, в котором система будет работать; корпус компьютера адаптирован под место установки; процессоры подобраны с учетом используемых алгоритмов и оптимизации энергопотребления; есть дополнительное экранирование и т.п.);
- требования реального времени, связанные с тем, что компьютер должен работать не в своем "компьютерном времени", а в физическом, а значит, у нас есть конкретные требования о том, что и когда должно происходить.

Рассмотрим пример, когда одна и та же программа может выступать как в роли информационной системы, так и управляющей: видеокодек. Если он используется для преобразования видеофайла между форматами для публикации в интернете, то это типичный пример информационной системы, для которой время выполнения не принципиально (чем быстрее, тем лучше, но и за ночь тоже сойдет). А если он используется для видеоплеера, то это уже система реального времени, и скорость преобразования файла в видеопоток должна соответствовать количеству кадров в секунду, так как в случае слишком быстрого, медленного или скачущего воспроизведения насладиться фильмом не получится.

Ниже на рисунке показана эволюция систем управления (ИУС) в направлении растущей интеграции с объектом управления и киберфизических систем (КФС). Также можно видеть, насколько "глубоко" и "широко" затрагивается элементная база компьютерных систем, насколько сильно необходимо включать в работу вопросы проработки "аппаратной составляющей".

![_Эволюция вычислительной компоненты КФС (ИУС -- информационно-управляющие системы, ВК -- вычислительная компонента)_ ](../fig/CPS-elements-evolution.png){#fig:CPS-elements-evolution}

[Изображение][^ref-pae-2021]

## Понятие системы и жизненный цикл системы [^ref-cpo]

Понятие системы является одним из фундаментальных понятий в инженерии и науке. Это понятие учитывается дисциплиной системной инженерии, направленной на создание успешной системы.

Системная инженерия (SE)
: это междисциплинарный подход и средство, позволяющее реализовать успешные системы. Он фокусируется на целостном и одновременном понимании потребностей заинтересованных сторон (стейкхолдеров); изучении возможностей; документировании требований; и синтезе, проверке, приемке и разработке решений при рассмотрении всей проблемы, от исследования концепции системы до вывода системы из эксплуатации.
: --- The Guide to the Systems Engineering Body of Knowledge (SEBoK), V. 1.3. 2014.

Systems Engineering (SE)
: is an interdisciplinary approach and means to enable the realization of successful systems. It focuses on holistically and concurrently understanding stakeholder needs; exploring opportunities; documenting requirements; and synthesizing, verifying, validating, and evolving solutions while considering the complete problem, from system concept exploration through system disposal.
: --- The Guide to the Systems Engineering Body of Knowledge (SEBoK), V. 1.3. 2014.

Чтобы полностью описать вычислительный процесс и компьютерную систему, мы должны кратко рассмотреть понятие системы с трех точек зрения: внутренняя организация, операционное окружение и жизненный цикл.

### Внутренняя организация

Классическое определение термина "система" с двумя примечаниями из стандарта звучит так:
система
: комбинация взаимодействующих элементов, организованных для достижения одной или нескольких поставленных целей

: ПРИМЕЧАНИЕ 1 Система может рассматриваться как продукт или как предоставляемые ею услуги.
: ПРИМЕЧАНИЕ 2 На практике интерпретация ее значения часто уточняется использованием ассоциативного существительного, например, авиационная система. В качестве альтернативы слово "система" может быть заменено просто контекстно-зависимым синонимом, например "самолет", хотя это может затруднить восприятие системных принципов.

System
: a combination of interacting elements organized to achieve one or more stated purposes

: NOTE 1 A system may be considered as a product or as the services it provides.
: NOTE 2 In practice, the interpretation of its meaning is frequently clarified by the use of an associative noun, e.g. aircraft system. Alternatively the word system may be substituted simply by a context dependent synonym, e.g. aircraft, though this may then obscure a system principles perspective.

Ключевые моменты этого определения гласят, что система -- это конструкция из взаимодействующих компонентов (см. [рисунок](#fig:modeling-complex-system-composition)). Для любой компьютерной системы этими компонентами являются полупроводники, конденсаторы, резисторы, триггеры и т.д., но этот набор элементов бесполезен, если только вы не создадите интегральную схему для конкретного приложения (ASIC). Кроме того, она включает в себя множество информационных компонентов, таких как программы, данные и т.д.

![_Структуру системы можно смоделировать в виде перевернутого дерева _](../fig/modeling-complex-system-composition.png){#fig:modeling-complex-system-composition}

[Изображение][^ref-gielingh]

Теоретически, мы можем проникнуть глубоко внутрь системной организации до элементарных частиц, но это не только бесполезно, но и вредно. Выбор детализации компонентов, уровня абстракции и вычислительных платформ имеет решающее значение при разработке компьютерных систем. Этот выбор должен соответствовать компьютерной системе и бизнес-требованиям. Например, если вы создаете веб-приложение, вы должны работать над веб-платформой с компонентами веб-страниц; если вы создаете встроенный контроллер, вы можете работать на уровне чистого металла с контроллером, его периферией. Если разработчики создадут систему на слишком низком уровне, это повысит сложность и стоимость разработки системы. Если разработчики попытаются создать систему на высоком уровне, реализация системы будет избыточной и может не соответствовать требованиям.

### Операционное окружение

Как мы уже говорили, компьютерная система разрабатывалась для ее использования во время выполнения. Её ключевым свойством является её функциональность. Функциональные возможности также зависят от операционных сред, в которых люди, другие системы или физические объекты взаимодействуют с интересующей системой (см. [рисунок](#fig:operational-environment-and-enabling-systems), что такое обеспечивающая система, будет описано в следующем параграфе).

![_Представляющая интерес система, ее операционная среда и обеспечивающие системы_ ](../fig/operational-environment-and-enabling-systems.png){#fig:operational-environment-and-enabling-systems}

[Изображение][^ref-iso15288]

На этом этапе мы можем спросить об идентификации системы (что это за система? Какова граница системы? Где мы ограничиваем нашу ответственность за систему?). Давайте посмотрим пример на [рисунке](#fig:space-time-map-of-the-chairman).

![_Пространственно-временная карта председателя банка NatLand _](../fig/space-time-map-of-the-chairman.png){#fig:space-time-map-of-the-chairman}

[Изображение][^ref-partridge]

Мы видим, что в начале мистер Джонс является председателем банка NatLandBank, но в конце его заменяет мистер Смит. Вопрос: "Является ли председатель банка в начале и в конце одним и тем же?" Ответ зависит от ваших соображений и будет:

- "Нет", если у нас личные отношения с председателем;
- "Да", если у нас профессиональные отношения с председателем (или функциональное место председателя).

С точки зрения информационных технологий: "Если он ходит как утка и крякает как утка, то это должна быть утка" ([шутка](#fig:duckfooding)). Таким образом, для компьютерной системы операционная среда важнее, чем внутренняя организация. Если мы сохраним интерфейс для операционной среды, обычно мы можем полностью изменить внутреннюю организацию, и это никого не волнует. Например, посмотрите на современные мэйнфреймы, которые выполняют исходный код семидесятых годов. Что подразумевает использование перфокарт, но работает с высокоскоростным твердотельным накопителем.

![_Если он ходит как утка и крякает как утка, то это должна быть утка_](../fig/duckfooding.jpeg){#fig:duckfooding}

Главный вопрос анализа операционной среды звучит так: "Когда мы должны остановиться?". Потому что мы можем расширять её снова и снова, пока она не станет Вселенной. Системный инженер может найти ответ только в конкретном случае. ISO 15288 предлагает нам следующий пример для воздушных судов.

![_Стандартный системный обзор воздушного судна в среде его эксплуатации _](../fig/system-view-of-an-aircraft.png){#fig:system-view-of-an-aircraft}

[Изображение][^ref-iso15288]

### Жизненный цикл [^ref-cpo]

Системная организация и её операционная среда определяют действующую систему (или систему, представляющую интерес). Но этого недостаточно для успешной системы, которую следует разрабатывать, развертывать и использовать. Целостное представление должно включать жизненный цикл системы и её обеспечивающие системы (см. [рисунок](#fig:operational-environment-and-enabling-systems)).

Жизненный цикл системы
: эволюция интересующей системы со временем от концепции до вывода из эксплуатации

Обеспечивающая система
: система, которая дополняет интересующую систему на этапах ее жизненного цикла, но не обязательно вносит непосредственный вклад в ее функционирование во время эксплуатации.
: ПРИМЕЧАНИЕ 1 Например, когда система, представляющая интерес, вступает в стадию производства, требуется вспомогательная производственная система.
: ПРИМЕЧАНИЕ 2 Каждая обеспечивающая система имеет свой собственный жизненный цикл. Этот Международный стандарт применим к каждой обеспечивающей системе, когда она сама по себе рассматривается как система, представляющая интерес.
: --- ISO 15288 [[^ref-iso15288]]

System life cycle
: the evolution with time of a system-of-interest from conception through to retirement

Enabling system
: a system that complements a system-of-interest during its life cycle stages but does not necessarily contribute directly to its function during operation
: NOTE 1 For example, when a system-of-interest enters the production stage, an enabling production system is required.
: NOTE 2 Each enabling system has a life cycle of its own. This International Standard is applicable to each enabling system when, in its own right, it is treated as a system-of-interest.
: --- ISO 15288 [[^ref-iso15288]]

Давайте кратко рассмотрим типичные этапы жизненного цикла, которые представлены для каждой системы (подробнее см. [[^ref-iso15288]], Annex B]), но этого недостаточно для любого конкретного случая:

1. _Концептуальный этап_ выполняется для оценки новых бизнес-возможностей, разработки предварительных системных требований и приемлемого проектного решения. Для компьютерной системы это анализ требований и архитектурное проектирование.
2. _Этап разработки_ выполняется для разработки системы, представляющей интерес, которая отвечает требованиям покупателя и может быть создана, протестирована, оценена, эксплуатирована, поддержана и выведена из эксплуатации. Для компьютерной системы это написание исходного кода или организация вычислительного процесса.
3. _Этап производства_ выполняется для производства или изготовления продукта, тестирования продукта и создания соответствующих поддерживающих и обеспечивающих систем по мере необходимости. Для компьютерной системы это разработка аппаратного обеспечения, подготовка исполняемых артефактов и создание дистрибутивов.
4. _Этап утилизации_ выполняется для эксплуатации продукта, предоставления услуг в заданных условиях и обеспечения постоянной операционной эффективности.
5. _Этап поддержки_ выполняется для предоставления услуг по логистике, техническому обслуживанию и поддержке, которые обеспечивают непрерывную работу системы, представляющей интерес, и устойчивое обслуживание.
6. _Этап вывода из эксплуатации_ выполняется для отключения системы, представляющей интерес, и связанных с ней функциональных и вспомогательных услуг, а также для функционирования и поддержки самой выведенной из эксплуатации системы. Обычно для компьютерной системы этот этап включает подготовку к модернизации или замене.

Для получения более подробной информации см. пример жизненного цикла встроенной системы на [рисунке](#fig:ES-HW-SW-lifecycle), связанный с подходом к разработке аппаратного/программного обеспечения.

![_Схема жизненного цикла проектирования встроенной системы_](../fig/ES-HW-SW-lifecycle.png){#fig:ES-HW-SW-lifecycle}

> Совместное проектирование аппаратного и программного обеспечения исследует параллельное проектирование аппаратных и программных компонентов сложных электронных систем. Оно стремится использовать синергию аппаратного и программного обеспечения с целью оптимизации и/или удовлетворения проектных ограничений, таких как стоимость, производительность и мощность конечного продукта. В то же время оно нацелено на значительное сокращение сроков выхода на рынок.

Комплексное представление системы показано на [рисунке](fig:system-life-cycle).

![_Взаимодействие системы с типичными обеспечивающими системами_](../fig/system-life-cycle.png){#fig:system-life-cycle}

## Понятие архитектуры [^ref-cpo]

![_Архитектура_](../fig/geek-and-poke-architecture.jpg){#fig:geek-and-poke-architecture}

Одним из важнейших понятий проектирования компьютерной системы является "архитектура". Это одно из самых важных и запутанных понятий в компьютерной инженерии. Мы объясним, почему разработчики должны работать на архитектурном уровне и расскажем о базовых концепциях для достижения этой цели.

Прежде всего, нам нужно определить роль понятия архитектуры. Для этого мы начнем со следующего определения (мы не разделяем компьютерные и программные системы).

Архитектура
: все важное
: --- Интернет

Архитектура программного обеспечения
: это набор проектных решений, которые, если они будут приняты неправильно, могут привести к отмене вашего проекта.
: --- Eoin Woods (SEI 2010)

Software architecture
: is the set of design decisions which, if made incorrectly, may cause your project to be cancelled.
: --- Eoin Woods (SEI 2010)

Первое звучит как шутка, но последнее является ключом к пониманию архитектуры в процессе проектирования. Она должна быть реализована на ранней стадии и выполнена высококвалифицированными специалистами. Архитектура должна принимать все важнейшие решения для создания успешного проекта. Но с помощью этих определений мы можем анализировать то, что является частью архитектуры, только в ретроспективе, когда проект был успешным или неудачным.

Более подробное определение архитектуры можно взять из стандарта:

Архитектура
: (системы) фундаментальные концепции или свойства системы в ее среде, воплощенные в ее элементах, взаимосвязях и принципах ее проектирования и развития

Описание архитектуры
: рабочий продукт, используемый для воплощения архитектуры
: --- ISO 42010

Architecture
: (system) fundamental concepts or properties of a system in its environment embodied in its elements, relationships, and in the principles of its design and evolution

Architecture description
: work product used to express an architecture
: --- ISO 42010

Это определение рекомендуется для использования в разработке программного обеспечения и систем. Мы должны отметить следующие последствия этого:

1. Аппаратное и программное обеспечение являются компонентами компьютерной системы. Это означает, что при анализе компьютерной системы вы не можете абстрагироваться ни от одного из них. И нам нужно создать механизм связи между разработчиками программного и аппаратного обеспечения.
2. Понятие "архитектура" включает в себя технические вопросы и вопросы, связанные с проектом. Невозможно спроектировать архитектуру вычислительных систем только с технической точки зрения. Следующие вопросы также являются частью архитектуры:
    - команда и компетентность, например использование технологий, если по ним нет специалиста, значительно увеличивает риски;
    - время выхода на рынок: если внедрение системы займет несколько лет, существует высокая вероятность того, что она станет бесполезной по истечении этих сроков;
    - обслуживание системы, поддержка, развертывание и обновление;
    - и т.д.
3. Система имеет архитектуру вне зависимости от ее описания. В качестве примера вам необходимо добавить новые функциональные возможности в существующую систему без архитектурной документации. Существует два способа выполнения этих задач:
    - Добавьте новую функцию в систему методом "грубой силы". Обычно это можно быстро сделать за счет обеспечения согласованности системы, но, если это долговечный продукт, это значительно увеличит затраты на техническое обслуживание.
    - Проанализируйте архитектуру системы (если возможно) и интегрируйте новую функцию. Такой подход позволяет элегантно расширить существующую вычислительную систему, не внося в нее концептуальных несоответствий.

И последнее, классическое определение архитектуры программного обеспечения с точки зрения разработки программного обеспечения:

Архитектура
: Логическая и физическая структура компонентов системы и их взаимосвязи, сформированные всеми стратегическими и тактическими проектными решениями, применяемыми во время разработки.
: Логический взгляд на систему учитывает концепции, созданные в концептуальной модели, и устанавливает существование и роль ключевых абстракций и механизмов, которые будут определять архитектуру и общий дизайн системы. Физическая модель системы описывает конкретный программный и аппаратный состав реализации системы. Очевидно, что физическая модель зависит от конкретной технологии.
: --- Гради Буч [[^ref-booch]]

Architecture
: The logical and physical structure of a system's components and their relationships, forged by all the strategic and tactical design decisions applied during development.
: The logical view of a system takes the concepts created in the conceptual model and establishes the existence and meaning of the key abstractions and mechanisms that will determine the system's architecture and overall design. The physical model of a system describes the concrete software and hardware composition of the system's implementation. Obviously, the physical model is technology-specific.
: --- Grady Booch [[^ref-booch]]

Определение Гради Буча явно подразумевает преобразования компьютерной системы в логический и физический компонент. Логический компонент -- это та часть системы, которая необходима для ее применения. Физический компонент -- это обеспечивающая часть вычислительной системы, которая позволяет системе функционировать. Это хорошее определение, но немного устаревшее для современных реконфигурируемых и облачных систем, где аппаратное обеспечение может динамически изменяться в зависимости от текущих требований.

Ключевыми особенностями архитектуры системы являются ее многочисленные представления. В качестве типичного примера вы можете увидеть UML (Unified Modeling Languages), который включают в себя множество типов диаграмм для описания системы для различных целей.

На самом деле, нам нужно работать с архитектурой не только в форме спецификаций или документов. Также мы используем её в специальных схемах "на салфетках" или в системе мониторинга проблем, когда нам нужно поставить задачу в контексте нашей инженерной культуры. Следовательно, мы создаем множество архитектурных описаний "на лету" в разных контекстах и группах людей, что может вызвать множество вопросов. Давайте посмотрим на несколько примеров:

1. Менеджер ставит задачу разработчику ([рисунок](#fig:communication-problem-manager-and-dev)). Многоточиями мы представляем наборы вопросов. Сплошная линия представляет то, что знает менеджер. Когда он ставит задачу, он использует только часть своих знаний (синий). Разработчик придерживается другой точки зрения и а) обычно получает некоторую ненужную информацию и б) должен делать предположения о невысказанных вопросах (или спрашивать). Оба архитектурных решения необходимы для создания успешной системы.
    - ![_Проблема коммуникации между менеджером и разработчиком_](../fig/communication-problem-manager-and-dev.png){#fig:communication-problem-manager-and-dev}

2. Старший разработчик (senior) ставит задачу перед младшим разработчиком (junior) ([рисунок](#fig:communication-problem-senior-and-junior)). Теоретически, они должны иметь схожую сферу применения, и старший разработчик должен передавать свои знания младшему. Старший формулирует ключевой тезис младшему, а младший расширяет эту информацию до необходимых аспектов своей работы.

    Что обычно происходит на практике: старший формулирует ключевой тезис младшему, и он не может его должным образом расширить, потому что у него недостаточно компетенции. Младший может застрять на этой задаче надолго, потому что, пытаясь найти все необходимые вопросы, он будет детализировать свою область, а не расширять ее.

    - ![_Проблема коммуникации между старшим и младшим разработчиками_](../fig/communication-problem-senior-and-junior.png){#fig:communication-problem-senior-and-junior}

Качество такого общения сильно зависит от понимания процесса всей командой. Вред от неправильных предположений и людей, которые застряли без причины, может быть значительным, и его следует предотвращать как можно раньше. Методы системного проектирования и архитектурного проектирования нацелены на то, чтобы помочь людям решить все необходимые для достижения успеха вопросы.

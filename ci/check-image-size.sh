#!/bin/sh

images_path=${1:-fig}

echo "Looking for images larger than 750k..."
find $images_path -type f -size +750k -exec sh -c 'for file in "$@"; do echo "$file"; done && exit 1' sh {} +
if [ $? -eq 1 ]; then
  echo "Images larger than 750k are not allowed."
  exit 1
else
  echo "No images larger than 750k found."
  exit 0
fi

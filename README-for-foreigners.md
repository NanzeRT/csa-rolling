# Архитектура компьютерных систем для студентов из Китая

(2023, осень)

Все материалы курса доступны здесь: [computer-systems/csa-rolling](https://gitlab.se.ifmo.ru/computer-systems/csa-rolling/-/tree/master)

Ведомость иностранных студентов: [link](https://docs.google.com/spreadsheets/d/1X6mNnw5yDCfXgsRWKkGBLyjUK71mDwVv63_2y7p60II/edit?usp=sharing), лист foreigners.

## Лекции

1. Ссылки на записи лекционных занятий доступны тут: [README.md](https://gitlab.se.ifmo.ru/computer-systems/csa-rolling/-/blob/master/README.md). Раздел: "Лекции (записи)".
1. Новые записи выкладываются еженедельно.
1. Слады лекций: <http://csa.edu.swampbuds.me>

## Лабораторные занятия

Вам необходимо:

1. Выполнить лабораторную работу 1 и/или 3.
1. Выполнить лабораторную работу 2.

Полные версии заданий доступны тут: [README.md](https://gitlab.se.ifmo.ru/computer-systems/csa-rolling/-/blob/master/README.md)

Особенности проведения лабораторных работ для иностранных студентов:

### Лабораторная работа 1

1. Тема и содержание доклада согласуются с преподавателем через Телеграм в личной переписке. Тема -- вносится в ведомость иностранных студентов. Приводить вопрос в описании темы не требуется.
2. Доклад готовится и предоставляется видеозаписью. Продолжительность 20-30 минут.
3. После ознакомления с докладом преподаватель подготовит список вопросов, ответы на которые будет необходимо подготовить и предоставить в текстовом виде. После этого лабораторная работа может считаться сданной.

### Лабораторная работа 2

1. Темы для эссе публикуются тут: [README.md](https://gitlab.se.ifmo.ru/computer-systems/csa-rolling/-/blob/master/README.md)
1. Проверка ваших эссе будет выполняться преподавателем.
1. Эссе предоставляются преподавателю через Телеграм.
1. Лабораторная работа считается сданной после рецензирования трёх ваших эссе.

### Лабораторная работа 3

1. Присваивание варианта осуществляется по запросу студента. Он вносится в ведомость.
1. Защита разработанного проекта осуществляется на консультации. Консультация назначается по вашему запросу.

## Экзамен

Для допуска к экзамену вам необходимо:

- сдать 2 лабораторные работы.

Особенности проведения лабораторных работ для иностранных студентов:

- экзаменационный билет состоит из 2 вопросов по основному материалу курса (лекции).
- формат экзамена: подготовка ответа на билет и его устная защита.

Список вопросов на подготовку экзамена: [questions.md](https://gitlab.se.ifmo.ru/computer-systems/csa-rolling/-/blob/master/questions.md), раздел: "Для всех групп"

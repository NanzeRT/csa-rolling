# Issue 1

## From: Алена Ефремова

Система оценивания по дисциплине состоит из 3 лабораторных и экзамена. Студенты пишут в обращении, что первая и вторая лабораторные заключались в докладе и трех эссе, и оценивались одногруппниками без каких-либо критериев (т.е. на усмотрение студентов). Третья лабораторная была введена преподавателем в середине семестра (отсутствует в БаРС), при этом на нее отведено наибольшее количество баллов. Однако ее хоть и принимает сам лектор, но в порядке очереди, и не все успевают получить баллы.

Вместе с тем оценка за экзамен студентам проставлялась не сразу после ответа, а только после того, как сдал весь поток.

В итоге после экзамена большая часть обучающихся направлена на ППА.

---

## From: Aleksandr Penskoi

Добрый день.

Как обычно бывает в таких случаях, в замечаниях студентов не совсем соответствуют действительности. Разберу по пунктам:

1) "Система оценивания по дисциплине состоит из 3 лабораторных и экзамена.” — Всё верно.

2) “Студенты пишут в обращении, что первая и вторая лабораторные заключались в докладе и трех эссе, и оценивались одногруппниками без каких-либо критериев (т.е. на усмотрение студентов).” — неполная информация.

Лабораторная работа №1:

- Оценка преподавателя: до 14 баллов.
- Оценка участия в лабораторных работах (выставляется преподавателем): 10 баллов.
- Оценка от других студентов: от 0 до 6 баллов.

Лабораторная работа №2:

- Оценка за выполнение задания: 3 баллов.
- Оценка за рецензирование эссе других студентов: 9 баллов (анонимное, каждый студент получает 9 рецензий).
- Оценка на основании рецензий другими студентами: 3-9 баллов.

Как можно видеть, баллы выставляемые на основании взаимной проверки работ студентами могут повлиять на оценку студента максимум на 12 баллов. С учётом того, что за курс студент максимум может набрать 121 балл — считаю допустимым рассматривать это как допустимую погрешность (109 баллов студент может получить взаимодействуя только с преподавателем).

3) “Третья лабораторная была введена преподавателем в середине семестра (отсутствует в БаРС), при этом на нее отведено наибольшее количество баллов.” — не соответствует действительности.

- Информация о лаб. 3 была доведена до студентов в сентябре месяце, как и количество баллов за неё (решение о ней было принято совместно с деканом, чтобы “усилить” данный курс).
- Варианты по лаб. 3 были выданы учащимся в середине ноября. Дата выдачи вариантов была обусловлена лекционным планом курса (знания с лекций прямо используются в лаб. 3).
- Лаб. 1 — 30 баллов максимум. Лаб. 2 — 21 балл максимум. Лаб. 3 — 30 баллов максимум. Экзамен — 40 баллов максимум.

4) “Однако ее хоть и принимает сам лектор, но в порядке очереди, и не все успевают получить баллы.” — неполная информация.

- До нового года лаб. 3 была показана не более чем 20 студентам (поток — 150+).
- В течении сессии была как минимум одна консультация по лаб. 3, на которую не явился ни один студент. Дата консультации была известна где-то за неделю.
- Сейчас, 27.01.2023 года, уже после всех экзаменов преподаватель по дисциплине прервал консультацию по лаб. 3 для написания настоящего письма.

5) “Вместе с тем оценка за экзамен студентам проставлялась не сразу после ответа, а только после того, как сдал весь поток.” — Всё верно, но:

- Учащимся предлагалась возможность посетить консультацию после экзамена в случае, если он не согласны с полученной оценкой и хотят получить объяснение оценки. Это возможностью воспользовались единицы учащихся.
- Отсутствие результата экзамена сразу после ответа на вопрос был призван исключить “препирательство” со стороны студентов и стало вынужденной мерой из-за крайне высокой нагрузки. Большинство групп сдавала экзамен по 2 группы за один день, вследствие чего экзамен длился порядка 10-12 часов. Просьба не проводить два экзамена в день по данной дисциплины озвучивалась в процессе планирования расписания сессии, но была проигнорирована.

6) "В итоге после экзамена большая часть обучающихся направлена на ППА.” — не соответствует действительности.

- 83 студента — набрали баллов для успешного закрытия сессии.
- 50 студентов (все сдали экзамен) — не набрали нужного количества баллов.
- подавляющее большинство студентов не сделало и не сдавало лаб. 3.

С чем связана сложившаяся ситуация, по моему мнению: студенты переоценили свои силы и решили не сдавать лаб. 3 в надежде набрать желаемое количество баллов за счёт других активностей. Когда после сдачи экзамена стало очевидно, что набрать нужное количество баллов без лаб. 3 невозможно — им банально не хватило времени на её реализацию (лаб. 3 весьма объемна и требует большого количества времени даже для сильных студентов).

7) Кроме того, студентам была предоставлена возможность набрать дополнительные баллы за помощь в улучшении качества учебных материалов. Многие ей воспользовались.

С уважением, Пенской Александр.

PS: возвращаюсь на консультацию принимать лаб. 3 у тех ребят, которые её сделали в последний момент.

---

## From: Алена Ефремова

Александр Владимирович, добрый день!

Хотели бы уточнить несколько моментов для ответа студентам по обращению:

п.2 - возможно, что у студентов не было развернутых критериев оценки лабораторных, поэтому у них возникло некоторое недопонимание.

Вместе с тем с учетом большого потока обучающихся в общих случаях не рекомендуется делать акцент на качественной оценке, в частности для повышения эффективности системы оценивания увеличивается доля количественных оценок, вводится тестирование.
Коллега из отдела образовательных технологий, Екатерина Сергеевна Джавлах (в копии), готова подключиться к пересмотру системы оценивания по дисциплине.

п.3 — есть ли какие-то скриншоты, которые могут подтвердить факт информирования студентов о лабораторной № 3 (сроки ее закрытия, разбалловка, уведомление о консультации). Это стало бы весомым аргументом в ответ обучающимся.

Вы указываете, что студенты могли набрать максимум 121 балл. Как в данном случае баллы конвертировались в 100: нормировались или сколько студент набирал, столько и переводили в отметку? В пользу ли студентов увеличение максимального количества баллов до 121?

п.5 мы передадим коллегам из диспетчерского центра и обратим их внимание на то, что ваша просьба не проводить два экзамена в день не была осуществлена.

Заранее спасибо за ответ!

---

## From: Aleksandr Penskoi

> Вместе с тем с учетом большого потока обучающихся в общих случаях не рекомендуется делать акцент на качественной оценке, в частности для повышения эффективности системы оценивания увеличивается доля количественных оценок, вводится тестирование.

Полностью с вами согласен. Переход на количественные оценки, вместо качественных, позволит значительно сократить нагрузку на ППС. Одновременно с этим, курс станет проще и понятнее для студентов: они сдают набор простых заданий на конкретный, фиксированный набор знаний и полностью закрывают дисциплину.

К сожалению, курс “Архитектура компьютера” является одним из основополагающих курсов учебных программ нашего факультета. Его задача не ограничивается получением набора конкретных знаний по конкретным архитектурам и принципам построения компьютеров. Дисциплина ставит перед собой задачу показать междисплинарность в компьютерных системах. Как самые разные разделы информатики и вычислительной техники переплетаются между собой в современных компьютерных системах. Как они влияют друг на друга самым непредсказуемым образом. Как они строятся от простейших примитивов до современных сверхсложных систем. Показать, как область интереса конкретного студента (информационная безопасность, программирование, искусственный интеллект, аппаратное обеспечение, системы управления и т.п.) находит отражение в фундаментальных вопросах организации компьютеров.

Что бы достигнуть этого, в рамках дисциплины архитектуры компьютера приходится прибегать к заданиям, отражающим индивидуальные интересы студентов (выбор и согласование тем по лаб. №1), а значит вести индивидуальную работу с каждым студентом, на сколько это позволяет огромный размер потока. А также прибегать к комплексным заданиям (речь о лаб. №3), без которых целостную картину области без разделения на “программистов и аппаратчиков” невозможно ощутить, так как никакой другой курс ввиду своей специализации не позволяет охватить вопрос достаточно широко.

Также отмечу, курс включает в себя тестирование для проверки усвоения теоретического материала. Данные тесты направлены на ранее выявление “неудачно поданных тем”, а не на формирование итоговой оценки за курс, что в случае обнаружения устранить проблему не дожидаясь экзамена.

Добавил в копию письма нашего декана, Кустарёва П.В., с которым утверждалась концепция курса.

> п.3 — есть ли какие-то скриншоты, которые могут подтвердить факт информирования студентов о лабораторной № 3 (сроки ее закрытия, разбалловка, уведомление о консультации). Это стало бы весомым аргументом в ответ обучающимся.

Данный курс ведётся с той степенью прозрачности, на которую я только способен, поэтому я даже затрудняюсь привести какие-то конкретные “ссылки” или “скриншоты”:

- все организационные вопросы решаются в общем чате потока;
- текстовые материалы курса формируются и обновляются в реальном времени с использованием систем контроля версий, поэтому при желании можно отследить появление любой информации с точностью до минут;
- все лекции и ключевые консультации доступны на записи.

Поэтому я ограничусь двумя ссылками:

- Консультация, запись которой опубликована 9 ноября, на которой подробно разбирается задание лаб. №3 с примером: https://www.youtube.com/watch?v=ry343G1JK6c
- Лекция, запись которой опубликована 22 ноября, в начале которая я интересуюсь у учащихся их “успехами” по лаб. №3 и приглашаю к демонстрации результатов на консультациях: https://www.youtube.com/watch?v=BBOgjGAvWhw

Если необходимо что-то большее, с удовольствием свяжусь с вами через Zoom и мы вместе подберём лучшие подтверждения.

> Вы указываете, что студенты могли набрать максимум 121 балл. Как в данном случае баллы конвертировались в 100: нормировались или сколько студент набирал, столько и переводили в отметку? В пользу ли студентов увеличение максимального количества баллов до 121?

Метод конвертации определён в самом начале семестра: min(100, actual_score), к примеру: 121 -> 100, 90 -> 90. Это позволяет дать учащимся гибкость при прохождении курса в зависимости от личных предпочтений. К сожалению, большинство студентов отказалось выполнения лаб. №3 и переоценила свои возможности по другим заданиям. Прилагаю статистику по курсу (те, кто получил 0 баллов — не выполнил лаб. №3). Скриншот сделан на момент написания письма. Как мне кажется — комментарии излишни.

<registry-lab3-stat-2023-02-18.png>

С текущими графиками (они обновляются автоматически при внесении новых данных) можно ознакомиться по ссылке: https://docs.google.com/spreadsheets/d/10y5B7SDW7QwFv1Fxt8fnrW5QgQeCUTOvZvmYe4BpQf8/edit#gid=580192402

> п.5 мы передадим коллегам из диспетчерского центра и обратим их внимание на то, что ваша просьба не проводить два экзамена в день не была осуществлена.

Благодарю вас!

---

## From: Павел Валерьевич Кустарёв

Добрый день.

Коль скоро Александр Владимирович сослался на меня, то подтверждаю в целом описанную им концепцию курса «Архитектура ЭВМ».
Мы понимаем проблемы «качественной оценки» и двигаемся в сторону большей «автоматизации» ее формирования (в смысле применения формальных и компьютеризованных методик и технологий поддержки оценивания). Но процесс этот небыстрый и пока более исследовательский, нежели технологический (и это международная практика).

А предлагаемые «стандартные» технологии количественной оценки типа тестирования не позволяют, к сожалению, адекватно оценивать даже компетенции студентов (в лучшем случае дают оценить только ЗУН), не говоря уже о комплексной оценке уровня профессиональной зрелости и дальнейшей профессиональной ориентации.

Но, как бы то ни было, мы оттачиваем подходы и относительно небольшое количество тревог студентов на фоне огромного контингента говорит , что мы движемся в нужном направлении:)

С уважением, Кустарёв П.В.

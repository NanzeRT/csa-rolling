# Журнал лабораторных занятий

## Ограничения на темы

Некоторые темы очень популярны, поэтому не стоит их выбирать, если только вы не уйдёте глубоко в какой-то частный и интересный вопрос:

- Ariane 5
- Boeing 737 Max
- Equifax, Apache Struts
- Log4Shell
- Log4j
- Single event upset
- Therac -- 25
- UUS Yorktown
- WannaCry, Microsoft SMB, EternalBlue
- Y2K, 2000 год (если только не будет какой-то необычный контент)
- Деление в Intel-ах (если не готовы серьёзно про вопросы верификации аппаратуры говорить)
- Патриот
- Уязвимость Heartbleed в OpenSSL
- Червь Морриса

Темы, в которых, по-видимому, нет технической глубины (к примеру, опечатка в коде), а мораль сводится к тому, что нужно тестировать много и по-разному. Тему можно подать, если будет глубокая мораль.

- Knight Capital -- сам по себе баг -- стечение беспечности и косяков. Непонятно, что полезное можете рассказать.
- Mariner 1 -- если проблема только в прочтении спецификации. Непонятно, что полезное можете рассказать.
- Nocom Minecraft Exploit 3 -- частная проблема. Непонятно, что полезное можете рассказать.

Если вы подаёте одну из перечисленных тем, то из описания должно быть очевидно, почему она будет интересна.

## Доклады

1. [Леденцов, Кравцова | 2023.09.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/Vsr7h7zBaTQ)

    1. Уязвимость Spring4Shell (Леденцов Дмитрий Андреевич)
    2. MCAS и аварии Boeing 737 Max (Кравцова Кристина Владимировна)

1. [Комлев, Кривошеев |, 2023.09.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/Nj3EAknDofQ)

    1. rm -rf moment. Или как удалили целую базу данных gitlab'а. (Комлев Игорь Владимирович)
    2. ILOVEYOU – вирус, который любит (Кривошеев Андрей Александрович)

1. [Гурьянов, Лушникова, Русинов | 2023.09.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/UwViPWey6XE)

    1. Stuxnet (2010) — вирус, атаковавший ядерные объекты (Гурьянов Кирилл Алексеевич)
    2. Устройство консенсуса блокчейна: понимание атаки 51% и стратегий предотвращения. (Лушникова Анастасия Сергеевна)
    3. Apache struts CVE-2017-5638 leads to RCE или очередная история про санитайзинг user-input (Русинов Дмитрий Станиславович)

1. [Зинатулин, Тучков | 2023.10.03 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/DEbnlNEWdEs)

    1. Разбор полетов - как ошибки в программном обеспечении привели к крушению тяжелой ракеты-носителя “Ariane 5” (Зинатулин Артём Витальевич)
    2. Атака с нулевым кликом или как старый софт может вставить палки в колёса современному приложению (Тучков Максим Русланович)
        - [В текстовом виде](https://github.com/IndianMax03/computer-architecture/blob/main/README.MD)

1. [Искандаров, Обляшевский, Лысенко | 2023.10.03 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/sHTIKT1HZ7k)

    1. Уязвимость Log4j CVE-2021-44228 (Искандаров Шахзодбек Хусаинович)
    2. Негативные стороны обратной совместимости - как крупные компании усложняют аппаратное и программное обеспечение, поддерживают толстые слои легаси, а иногда намеренно портят софт ради сохранения клиентов. (Обляшевский Севастьян Александрович)
    3. The DAO Hack (2016). Устройство проблемы повторного входа в блокчейне, причины возникновения данной проблемы и способы борьбы с ней. Структура смарт-контрактов. (Лысенко Артём Константинович)

1. [Барабанщиков, Волненко, Осокина | 2023.10.10 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/OqQB2_CZfnU)
    1. Уязвимость Shellshock в Bash (Барабанщиков Андрей Дмитриевич)
    2. Therac-25: когда код становится смертельным (Волненко Дмитрий Александрович)
    3. Инверсия приоритетов в системе Mars Pathfinder. Аппараты, которые смогли. (Осокина Мария Юрьевна)

1. [Трофимченко | 2023.10.10 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/opRK0dRPpRs)
    1. Системный дизайн (Вечный поиск архитектурно - оптимального решения) (Трофимченко Владислав Михайлович)

1. [Бусыгин, Рахматуллин, Лазеев | 2023.10.10 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/iw1PjJP1ptA)
    1. "Как ошибка в точности вычисления времени стала причиной смерти 28 военных" (Бусыгин Дмитрий Алексеевич)
    2. Как 43 секунды без связи привели к 24 часам деградации GitHub (Рахматуллин Рамазан)
    3. Разработчик из GitLab случайно удалил данные из продуктовой базы данных (Лазеев Сергей Максимович)

1. [Тройникова, Иванов, Соловьев | 2023.10.10 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/TTlnRqrAJPc)
    1. “Path Traversal” - старая уязвимость, которая до сих пор в топе CWE. (Тройникова Вероника Дмитриевна)
    2. XSS атаки (червь samy, который изменил интернет) (Иванов Никита Денисович)
    3. Log4Shell (CVE-2021-44228). RCE в Java - фича или баг? (Соловьев Павел Андреевич)

1. [Девяткин | 2023.10.17 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/Rv5hied1t7Q)
    1. Cross-region failover гитхаба 21.10.18 (Девяткин Арсений Юрьевич)

1. [Мозговая, Громилова, Кириллов | 2023.10.17 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/P8rcMGbwWt4)
    1. Крупнейший сбой в работе Facebook 2021 года (Мозговая Лариса Андреевна)
    2. Meltdown (Громилова Мария Дмитриевна)
    3. Patriot Missile Failure (Кириллов Андрей Андреевич)

1. [Цю, Верещагин | 2023.10.17 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/chpp4liPLUg)
    1. Первая кровь автопилота (Uber) (Цю Тяньшэн)
    2. Формат GIF как zero-click эксплойт в IOS (Верещагин Егор Сергеевич)

1. [Шпренгер, Кочнев, Аталян | 2023.10.24 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/ZkCBr_foig0)
    1. Проблемы монотонности и синхронизации часов в компьютерах и распределённых системах. (Шпренгер Константин)
    2. Доклад "Инцидент с Mars Climate Orbiter (Кочнев Роман)
    3. Проблемы работы со строками при написании многоязычных приложений. (Аталян Александр Эдуардович)

1. [Панин, Андриенко | 2023.10.24 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/_tLvUCYTpPg)
    1. Pentium fdiv bug (Панин Иван Михайлович)
    2. Основные проблемы синхронизации между клиентами и серверами в сетевых играх. Nocom Minecraft exploit. (Андриенко Сергей Вячеславович)

1. [Виноградов | 2023.10.24 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/q5DrUwltdY4)
    1. Как некорректное регулярное выражение и отсутствие выстроенного процесса тестирования привело к полной деградации функционала сервиса Cloudflare. (Виноградов Глеб Дмитриевич)

1. [Смирнов | 2023.10.24 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/AlZsDtkQXWk)
    1. Ручная работа с памятью. Как обеспечить безопасную работу? (Смирнов Виктор Игоревич)

1. [Волковская, Варюхин | 2023.10.24 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/IwmsPUFu6sk)
    1. Название: Событийно-ориентированная архитектура (EDA) как решение типовых проблем разработки микросервисов. (Волковская Ульяна Павловна)
    2. Повышение производительности баз данных. Проблема выбора оптимальной стратегии индексации. Почему дерево - не решение всех проблем. (Варюхин Иван Антонович)

1. [Скворцова, Клюева | 2023.10.31 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/dkcYHvKNL7E)
    1. Уязвимость MS17_010 (EternalBlue) (Скворцова Дарья Алексеевна)
    2. Как ошибка в регулярном выражении привела к падению сервисов Cloudflare по всему миру (Клюева Яна)

1. [Мориков | 2023.10.31 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/R4a4y69Hp4s)
    1. Как программный код при помощи машин начал убивать людей ещё в 1985 году. Therac-25. (Мориков Иван Дмитриевич)

1. [Понамарев | 2023.10.31 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/dUMespiRrhA)
    1. Тема: Проблемы шифрования криптовалюты или как взломать биткоин-кошелёк. Атака 51%, Двойная трата, Эгоистичный майнинг и другие. (Понамарев Степан)

1. [Афанасьев | 2023.10.31 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/NxesslUAF_g)
    1. Ошибка в работе системы крейсера Yorktown. (Афанасьев Даниил Олегович)

1. [Лебедев, Соболев, Хайкин | 2023.10.31 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/RcvLH-a3Ln4)
    1. Single Event Upset, влияние ионизирующего излучения на вычислительную технику (Лебедев Вячеслав)
    2. Атака «ручкой» или как KNOB-атака способна взломать Bluetooth и украсть ваши данные. (Соболев Иван Александрович)
    3. NULL - как с ним борются в языках программирования (null safety) (Хайкин Олег Игоревич)

1. [Емельянов | 2023.11.01 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/IwEvuu2rxRQ)
    1. Большие данные, большие проблемы: как решить проблему с масштабированием бд на примере Discord? (Емельянов Дмитрий Сергеевич)

1. [Бобрусь, Лагус | 2023.11.07 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/T5b50beb0jo)
    1. Опасность генерации случайных чисел (Бобрусь Александр Владимирович)
    2. Невозможность exactly-once доставки в распределенных системах (Лагус Максим Сергеевич)

1. [Привалов | 2023.11.07 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/pBzwuZsgF6M)
    1. SQL-инъекции или как гонять по базам данных. (Привалов Ярослав)

1. [Овчаренко, Рябоконь | 2023.11.07 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/pZ3oIswK5kM)
    1. 5 млн долларов за строчку кода: как сервисы Datadog были недоступны из-за обновления безопасности ОС (Овчаренко Александр Андреевич)
    2. Название доклада: "Уязвимости Inception и Downfall: Угрозы для современных процессоров AMD и Intel." (Рябоконь Архип Борисович)

1. [Исупов, Марченко | 2023.11.14 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/SPA8r9XgVfA)
    1. Остановка заводов Toyota из-за переполнения дискового пространства. (Исупов Денис Васильевич)
    2. Название доклада: Глобальный сбой в работе Facebook в 2021 году. (Марченко Анна Сергеевна)

1. [Гиниятуллин, Бекмухаметов, Кондратьева | 2023.11.14 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/cvxbNaE8fn0)
    1. Проблемы производительности и доступности баз данных в построении высоконагруженных приложений. Масштабирование через шардирование, репликацию и партиционирование. (Гиниятуллин Арслан)
    2. VBA, макросы и макровирусы (Бекмухаметов Владислав Робертович)
    3. Уязвимость сетевого протокола Microsoft SMB - EternalBlue  (Кондратьева Ксения Михайловна)

1. [Миху | 2023.11.14 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/IzKUNMBjhA4)
    1. Zenbleed(CVE-2023-20593), или где проходит граница между hardware и software. (Миху Вадим Дмитриевич)

1. [Гончаров, Шевчук | 2023.11.14 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/Lq2gh2tLI1Y)
    1. Проблема хранения даты и времени в памяти. (Гончаров Андрей Викторович)
    2. Поиск в тексте и полнотекстовый поиск в современных базах данных (Шевчук Дмитрий Олегович)

1. [Овсянников, Скрябин | 2023.11.15 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/N9MTYpekL9A)
    1. DDoS-атака (Овсянников Роман Дмитриевич)
    2. Как система обработки платежей Stripe с надежностью 99.999 отключилась на 2 часа. (Скрябин Иван Александрович)

1. [Ри | 2023.11.21 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/LaHUqHalyJI)
    1. Название доклада: Как Singal чуть не утратил статус безопасного мессенджера или почему важно думать при настройке TLS туннелей (Ри Аркадий Русланович)

1. [Юнусов, Абдурасул | 2023.11.21 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/IrKfujCniuU)
    1. Методы DRM защиты (Юнусов Роман)
    2. История кодировок: Откуда такой зоопарк? (Абдурасул Кызы Мээрим)

1. [Мария, Караулова, Анастасия | 2023.11.21 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/m9i3ctDfMy4)
    1. Форк Ethereum и инцидент с The DAO: проблемы безопасности смарт-контрактов и их последствия (Мария Алексеевна Вацет)
    2. Название: Когда сервера не смогли договориться или инцидент Github от 21 октября 2018 (Караулова Анастасия Павловна)
    3. Выбор архитектуры для мобильного iOS приложения (Анастасия Бегинина)

1. [Черемнов, Степанов, Дорожкин | 2023.11.21 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/SrS-QS-_nkE)
    1. Взлом Heartland или как SQL-инъекция может привести к утечке более 100 миллионов банковских карт (Черемнов Константин Дмитриевич)
    2. Название доклада: "Проблема Y2K: Как компьютерные системы избегли милленниумской катастрофы" (Степанов Артур Петрович)
    3. Название: "NoSQL инъекции на примере MongoDB" (Дорожкин Иван Кириллович)

1. [Агадилова | 2023.11.21 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/HD5ukgbBXAM)
    1. Break-астрофа: как простая строка кода сгубила 75 миллионов звонков (Агадилова Малика)

1. [Грибов, Бухаров, Батомункуева, Иванов | 2023.11.28 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/ovJnccu4KI8)
    1. Название: CSRF-атаки -- старая уязвимость, актуальная сегодня. (Грибов Михаил Олегович)
    2. Heartbleed (Бухаров Дмитрий Павлович)
    3. Авария Qantas72, Single Event Effect в авионике и компьютерных системах (Батомункуева Виктория Жаргаловна)
    4. SQL Slammer: Анатомия Эпохальной Кибератаки (Иванов Андрей Вячеславович)

1. [Поветин, Керпик, Пегушина | 2023.11.28 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/l0GT3e0-OR4)
    1. Самоуничтожение базы данных за 43 секунды или история о 24-х часовой недоступности GitHub (Поветин Илья)
    2. Сбой в Cloudflare 2 июля 2019 года (Керпик Артём Витальевич)
    3. Расширение Heartbeat для протоколов TLS и DTLS обеспечивает поддержку соединения активными без необходимости постоянного переподключения с полной авторизацией. (В докладе расскажу как оно работает) (Пегушина Екатерина)

1. [Мыц, Павлова, Горинов | 2023.11.28 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/81_iQx-nJBg)
    1. Крах Ethereum. The Parity Wallet Hack (Мыц Степан Евгеньевич)
    2. Как хакеры могут прослушивать ваши телефонные звонки через Bluetooth-гарнитуру или взлом безопасности Bluetooth (KNOB attack) (Павлова Алина Егоровна)
    3. Уязвимость Plundervolt в процессорах Intel (Горинов Даниил Андреевич)

1. [Чернова, Смирнова, Болдов | 2023.11.28 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/2vvoS1_rH2M)
    1. «Злые» регулярные выражения (ReDOS атака) (Чернова Анна Ивановна)
    2. Атака на цепочку поставок SolarWinds (2020) (Смирнова Алина Юрьевна)
    3. Rowhammer (Болдов Олег Евгеньевич)

1. [Миняев, Абульфатов | 2023.11.28 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/hhPcno67mpI)
    1. Исследование технических подробностей атаки на SolarWinds. (Миняев Илья Андреевич)
    2. Тема: Исправление уязвимости «нулевого дня» в Chrome (Абульфатов Руслан Мехтиевич)

1. [Тучин, Баранов, Дорморезов | 2023.11.29 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/ljFiWuThSsA)
    1. Блэкаут в США: как обвалилась целая энергосистема. (Тучин Артём Евгеньевич)
    2. The Essence Of Green Threads (Баранов Вячеслав Григорьевич)
    3. Datomic - функциональный подход к базе данных (Дорморезов Алексей)

1. [Баранов, Кравец, Клименко | 2023.12.05 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/6nCJTvFaDYs)
    1. Уязвимость в WinRAR (Баранов Денис Владимирович)
    2. Zero-day уязвимость Microsoft Support Diagnostic Tool(Follina). (Кравец Роман Денисович)
    3. Безопасность и эффективность в автономных автомобилях: Уроки из инцидента в системе управления трафиком. (Клименко Вячеслав)

1. [Лапин | 2023.12.05 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/rde9kOnvyj4)
    1. Dirty COW (CVE-2016-5195) - уязвимость ядра Linux (Лапин Алексей Александрович)

1. [Иванов, Пономарев, Караулова | 2023.12.05 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/1iTf8RJjR7o)
    1. LinkedIn Hadoop инцидент или как потеря 0.02% данных привела к масштабному сбою в важных бизнес-процессах компании. (Иванов Матвей Сергеевич)
    2. Serverless вычисления: новый этап эволюции разработки или дополнение к традиционным и облачным подходам? (Пономарев Иван Михайлович)
    3. Название: Когда сервера не смогли договориться или инцидент Github от 21 октября 2018 (Караулова Анастасия Павловна)

1. [Гасюк, Каплан, Эрбаев | 2023.12.05 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/PuEqWnmonkQ)
    1. Kerberos - как плохой генератор "случайных" чисел годами позволял злоумышленникам подбирать ключи к шифрованию (Гасюк Александр)
    2. Как архитектурная ошибка в MS Exchange привела к взлому 60 тысяч организаций по всему миру. (Каплан Дмитрий)
    3. Быть или не быть? Механизм глобальной блокировки интерпретатора Python - GIL. (Эрбаев Ильдус)

1. [Никашкин, Туляков, Дорофеев, Нуцалханов | 2023.12.12 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/Ow5NoYUapnE)
    1. Use after free и double free на примере Foxit PDF Reader и стандартной библиотеки Rust (Никашкин Алексей Валентинович)
    2. Log4Shell - уязвимость нулевого дня в Log4j. Полный разбор (Туляков Евгений Викторович)
    3. Название: Проблема 51%. Как с ней бороться и почему у криптовалюты невзрачное будущее. (Дорофеев Николай Павлович)
    4. GTA Online и sscanf() (Нуцалханов Нуцалхан)

1. [Аллаяров, Приходько, Загородников, Пушкин, Чаркин | 2023.12.12 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/ADWJRgQboEs)
    1. Средства верификации микропроцессоров (Аллаяров Игорь Олегович)
    2. Cloubleed Или Как Одна Строка Кода Почти Уничтожила Интернет (Приходько Максим Александрович)
    3. Как произошла 6ая по масштабу утечка данных во всем мире в компании Equifax. (Загородников Илья)
    4. Аппаратная уязвимость - Meltdown. (Пушкин Антон Сергеевич)
    5. Состязательное машинное обучение. Применение и защита. (Чаркин Виктор Сергеевич)

1. [Дау, Ушаков, Калинин, Полуянов, Базилов | 2023.12.12 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/Lvxn57tm6Sk)
    1. Sequoia: глубокие корни уровня файловой системы Linux (Дау Конг Туан Ань)
    2. Скрытое программное обеспечение: Уроки из истории XCP и Современные Тревоги в Области Инфобезопасности (Ушаков Максим Евгеньевич)
    3. Как разработчик удалил базу данных Gitlab (Калинин Даниил Дмитриевич)
    4. Разгадывая тайны Global Interpreter Lock: Влияние GIL на параллельное выполнение в Python. (Полуянов Александр Михайлович)
    5. Проблема зависимостей пакетов npm и безопасные способы работы с ними. (Базилов Дмитрий Валереьвич)

1. [Степаненко, Птицын, Воронина, Пономарев, Окечукву, Мухаметгалеев | 2023.12.12 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/u2Mo62vaz4w)
    1. Kubernetes - как локальная фича для Google стала популярнейшим в мире сервисом для развертывания и балансировки крупных проектов. (Степаненко Валерий Павлович)
    2. LegoOS: распределенная ОС для разделения аппаратных ресурсов (Птицын Максим Евгеньевич)
    3. Dirty Cow and dirty pipe - глупые названия серьезных уязвимостей в ядре Линукса. (Воронина Дарья Сергеевна)
    4. Уязвимость Heartbleed в OpenSSL (Пономарев Вадим)
    5. Сбой в работе Amazon Web Services (AWS) в 2017 году (Окечукву Александер Чуквуемека)
    6. Название: Мэрис - ddos атака на яндекс (Мухаметгалеев Даниил Тимурович)

1. [Кизилов, Воронин | 2023.12.12 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/NBk5BiBuVbo)
    1. «Додо Пицца» потеряла 8 миллионов за один час (Кизилов Степан Александрович)
    2. Технологии управления памятью в компьютерных системах.  (Воронин Иван Александрович)

1. [Беляков, Сафин, Никитин, Рудык | 2023.12.13 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/_yt0TNroB8o)
    1. Безопасны ли WIFI сети на самом деле? (Беляков Дмитрий Владимирович)
    2. Влияние Garbage Collector в языке C# на производительность и способы его оптимизации на примере игровой разработки. (Сафин Марат Радикович)
        - НЕ БЫЛО
    3. CSRF-уязвимости: анализ, методы защиты и их недостатки (Никитин Егор)
    4. Как регулярное выражение может положить все сервера Cloudfare и как этого избежать. (Рудык Ярослав Юрьевич)

1. [Ельмеев, Зенкевич, Демидович, Климович | 2023.12.19 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/gOM9HcBVgLI)
    1. Как обработка XML приводит к проблемам с безопасностью? (Ельмеев Ильдар)
    2. Инцидент для анализа: Ошибка Heartbleed в OpenSSL (Зенкевич Артем)
    3. Уязвимость Heartbleed в OpenSSL (Демидович Всеслав Игоревич)
    4. Кража данных используя XSS атаки и CORS уязвимости. Как защититься?  (Климович Вадим)

1. [Никита, Дробыш, Тюрин, Гулямов | 2023.12.19 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/r4PsAoLh_4A)
    1. Аудит/тестирование x86 процессоров. Нахождение аномалий.  (Никита Манжиков)
    2. Проблемы современного ML. (Дробыш Дмитрий Александрович)
    3. Роль процессора в работе ОС на примере xv6-riscv. (Тюрин Иван Николаевич)
    4. Как языки программирования пытаются работать со строками? (Гулямов Тимур)

1. [Шульга, Павлов, Шипунов, Плетнев, Марьин, Чесноков | 2023.12.19 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/1KM-QADBoJA)
    1. SQL vs ORM. Вечное идеологическое противостояние (Шульга Артём Игоревич)
    2. Червь Морриса и его влияние на инженерию компьютерных систем (Павлов Александр)
    3. Авария ракеты-носителя «Ариан-5» (Шипунов Илья Михайлович)
    4. Распределённое хранение данных: Оптимизация доступа и надёжности (Плетнев Александр Дмитриевич)
    5. Какие проблемы могут вызвать регулярные выражения? (Марьин Савва Сергеевич)
    6. Идеальная хэш-функция: по каким критериям подбирать и как она работает. (Чесноков Аркадий)

1. [Гиря, Сущенко, Баканова, Тороев | 2023.12.19 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО](https://youtu.be/p-_C6Wdx2n8)
    1. Отказ системы Amazon Web Services 2017: как простая ошибка сотрудника привела к крупной облачной катастрофе (Гиря Максим Дмитриевич)
    2. Dirty Cow или как выполнить любую команду с правами суперпользователя (Сущенко Роман Витальевич)
    3. Уязвимость биометрических характеристик для аутентификации или как заполучить отпечатки пальцев министра обороны Германии (Баканова Ирина Ивановна)
    4. Название: Колоночные и реляционные базы данных. Сравнение и анализ с точки зрения прозводительности. Ключевые особенности ClickHouse (Тороев Канатбек)

1. Романов, Чернова | 2023.12.19 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО (запись отсутствует)
    1. Чем отличается подход к разработке на Erlang от других ЯП? (Романов Артемий Ильич)
    2. Дорога в ад зависимостей. Технические способы разрешения конфликтов версионирования.  (Чернова Елизавета Александровна)

1. [Зачётная неделя, вт. 11:40 ↓ 13:10, Пенской](https://youtu.be/cjeP3F0GOk0)
    1. Руденко, Готовко, Новиков, Стрельбицкий, Калябина | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Взлом внутренней сети Target, приведший к утечке 40 миллионов кредитных карт: разбор ошибок в безопасности (Готовко Алексей)
        2. Spring4Shell: критическая уязвимость в Java-фреймворке Spring (Новиков Егор Сергеевич)
        3. Название доклада: "Stuxnet: компьютерный червь, который атаковал иранские ядерные объекты" (Калябина Александра Николаевна)
        4. Неудача Mars Climate Orbiter из-за использования разных единиц измерения. (Стрельбицкий Илья)
        5. SQL атака на Heartland Payment Systems (Руденко Илья Александрович)

1. [Зачётная неделя, вт. 13:30 ↓ 15:00, Пенской](https://youtu.be/FQw2Gqj1gRw)
    1. Петров, Киушкин | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Уязвимость через зависимости: Инцидент с NPM пакетом 'event-stream' (Киушкин Артем Эдуардович)
        2. Использование кеша при проектировании информационных систем и веб приложений (Петров Константин Игоревич)

    1. Погрибняк, Чебоксаров, Герасимов, Колесников | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Опасности бесконтрольного использования зависимостей в проектах Node.js (Погрибняк Иван Сергеевич)
        2. Уязвимость библиотеки OpenSSL. Heartbleed: cve-2014-0160. (Чебоксаров Ярослав)
        3. Ошибка високосного года в Microsoft Excel (Герасимов Артём Кириллович)
        4. Вирус 'Fractureiser' (Колесников Никита Алексеевич) (оценка 4)

1. [Зачётная неделя, вт. 11:40 ↓ 13:10, Горбачёв](https://youtu.be/Co91D2YA2-c)

    1. Журавлев, Гараев, Григорьев | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Sony и XCP (Журавлев Вячеслав Евгеньевич)
        2. Получение доступа к автомобилям Tesla с помощью Grafan’ы и уязвимости в TeslaMate (Гараев Раиль Фаннурович)
        3. Stuxnet: Первое кибероружие (Григорьев Андрей Сергеевич)

    1. Тюрин | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Сбой телескопа Хитоми из-за ошибок в алгоритмах системы ориентации. (Тюрин Святослав Вячеславович)

    1. Голиков, Ненов, Передрий | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Как одна фраза в стандарте даёт доступ ко всем Bluetooth устройствам это про knob атаку  (Голиков Денис)
        2. Как Signal скомпрометировал своих пользователей (Ненов Владислав)
        3. Редактирование значений в ячейках DRAM без прямого доступа к ним. Disturbance errors и rowhammer. (Передрий Михаил Сергеевич)

1. [Зачётная неделя, вт. 13:30 ↓ 15:00, Горбачёв](https://youtu.be/HXYiF7J74vo)

    1. Мартынов, Власенков, Вагаев, Бинов | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Зависимые типы (Мартынов Всеволод)
        2. Не ждём, а готовимся. Китайский файрвол и методы его обхода. (Власенков Андрей Игоревич)
        3. Спираль смерти для Terra — почему упали LUNA и UST (Вагаев Артем Ильдарович)
        4. Прерывание сервисов Cloudfare из-за ошибок в конфигурации маршрутизатора (Бинов Даниил Евгеньевич)

    1. Коновалов, Фирстова | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. MyDoom, или самый дорогой вирус в IT. (Коновалов Арсений Антонович)
        2. Как дополнительная секунда сломала Linux-системы (Фирстова Екатерина Витальевна)

    1. Шипулин, Павлова | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Основы ассемблерного программирования и его роль в разработке программного обеспечения.       (Шипулин Олег Игоревич)
        2. Как присвоение одной переменной NaN привело к аварии беспилотного автомобиля (Павлова Анастасия)

1. [Зачётная неделя, вт. 15:20 ↓ 16:50, Горбачёв](https://youtu.be/q96d7CITdRU)
    1. Королев, Пащенко, Орехов, Кудлай, Ярусова, Велюс, Черных | 2023.12.26 | Лаб. | АК-2023 | ПИиКТ | Университет ИТМО
        1. Взлом DAO в 2016. Сложности и риски разработки смарт контрактов на его примере. (Королев Денис Владимирович)
        2. NextGen система ERAM: взлеты и падения (Пащенко Олег Дмитриевич)
        3. Проблемы алгоритмов ML и их экстраполяции на разные типы данных. Проблема обобщения. (Орехов Сергей Владимирович)
        4. Single event effects (SEE) (Кудлай Никита Романович)
        5. Проблемы оптимизации видеоигр (Ярусова Анна Александровна)
        6. Название: Ошибка 01.01.1970 в 64-битных iOS-устройствах (Велюс Арина Костас)
        7. Выполнение удаленного кода из уязвимости SSTI в песочнице: автоматическое обнаружение и эксплуатация "template escape" (Черных Роман Александрович)
